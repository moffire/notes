from django.views.generic import View, CreateView, UpdateView, DeleteView, ListView
from django.shortcuts import render
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q, F
from landing.models import Post, Note
from landing.form import PostForm
from .filters import CategoryFilter


class PostList(ListView):
    model = Post
    context_object_name = 'post_list'
    template_name = 'base.html'
    paginate_by = 5
    queryset = Post.objects.filter(relevance=True).order_by('-post_created')

    def get_queryset(self):
        search_param = self.request.GET.get('search', None)
        filter_param = self.request.GET.get('categories_filter', None)

        queries = Q()
        if search_param or filter_param:
            if search_param:
                splitted_search_params = search_param.split()
                for s_param in splitted_search_params:
                    queries.add(Q(title__icontains=s_param), Q.OR)
                    queries.add(Q(content__icontains=s_param), Q.OR)
            if filter_param:
                splitted_filter_param = filter_param.split()
                for f_param in splitted_filter_param:
                    queries.add(Q(category__icontains=f_param), Q.AND)
            param_query = self.queryset.filter(queries)
            if param_query.exists():
                return param_query
        return self.queryset


    def get_context_data(self, **kwargs):
        categoty_list = Post.objects.all()
        kwargs['filter'] = CategoryFilter(queryset=categoty_list)
        return super(PostList, self).get_context_data(**kwargs)

class PostDetail(View):

    def get(self, request, slug):
        context = {}
        post = Post.objects.get(slug=slug)
        post.views += 1
        post.save()
        note_list = Note.objects.filter(post=post).order_by('note_created')
        context['note_list'] = note_list
        return render(request, 'landing/post_details.html', context)

class PostCreate(CreateView):
    form_class = PostForm
    model = Post
    template_name = 'landing/create_post.html'
    success_url = reverse_lazy('home_page')

    def form_valid(self, form):
        form.instance.author_id = self.request.user.id
        return super(PostCreate, self).form_valid(form)

class PostEdit(UpdateView):
    model = Post
    form_class = PostForm
    template_name = 'landing/post_edit.html'

    def form_valid(self, form):
        uploaded_file = self.get_form_kwargs().get('files')
        if uploaded_file:
            post_file = uploaded_file['file']
            form.file = post_file
        form.save()
        return super(PostEdit, self).form_valid(form)

    def get_success_url(self):
        if 'slug' in self.kwargs:
            slug = self.kwargs['slug']
            if self.object.category == 'url':
                return reverse_lazy('post_details', kwargs={'slug':slug})
            else:
                return reverse_lazy('home_page')
        else:
            return reverse_lazy('home_page')

class PostDelete(DeleteView):
    model = Post
    success_url = reverse_lazy('home_page')
    template_name = 'landing/delete_confirmation.html'
