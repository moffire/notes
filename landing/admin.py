from django.contrib import admin
from landing.models import Post, Note


class PostAdmin(admin.ModelAdmin):
    exclude = ['views', 'slug', 'author', 'image']
    list_filter = ['relevance', 'post_created']

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'author', None) is None:
            obj.author = request.user
        obj.save()

class NoteAdmin(admin.ModelAdmin):
    exclude = []

admin.site.register(Post, PostAdmin)
admin.site.register(Note, NoteAdmin)