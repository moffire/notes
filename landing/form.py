from django import forms
from landing.models import Post

class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ['category', 'title', 'content', 'link', 'file']

    category = forms.ModelChoiceField(queryset=Post.objects.values_list('category', flat=True).distinct(), to_field_name='category')
