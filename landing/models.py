import os
from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from django.conf import settings
from unidecode import unidecode

class Post(models.Model):
    author = models.ForeignKey(User)
    category = models.CharField(max_length=32, null=True, blank=True, verbose_name='Категория')
    title = models.CharField(max_length=128, blank=False, verbose_name='Заголовок')
    content = models.TextField(verbose_name='Контент', null=True, blank=True)
    post_created = models.DateTimeField(auto_now_add=True, verbose_name='Пост создан')
    link = models.URLField(verbose_name='Ссылка', null=True, blank=True)
    views = models.IntegerField(default=0, verbose_name='Просмотры')
    slug = models.SlugField(null=True, db_index=True)
    relevance = models.BooleanField(default=True)
    image = models.ImageField(default='media/no_image.jpeg')
    file = models.FileField(upload_to='uploaded_files', null=True, blank=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(unidecode(self.title))
        self.category = self.category.lower()
        category_image = self.category + '.jpeg'
        if os.path.exists(os.path.join(settings.MEDIA_ROOT, category_image)):
            self.image = category_image
        else:
            self.image = 'no_image.jpeg'
        super (Post, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'

class Note(models.Model):
    post = models.ForeignKey(Post)
    note_comment = models.TextField(verbose_name='Заметки')
    note_url = models.URLField(verbose_name='Ссылка', null=True, blank=True)
    note_created = models.DateTimeField(auto_now=True, verbose_name='Заметка создана')

    def __str__(self):
        return self.note_comment