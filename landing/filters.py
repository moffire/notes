from .models import Post
from django import forms
import django_filters

class CategoryFilter(django_filters.FilterSet):
    categories_filter = django_filters.ModelMultipleChoiceFilter(label='Filer By Category', queryset=Post.objects.values_list('category', flat=True).distinct(), widget=forms.RadioSelect, )

    class Meta:
        model = Post
        fields = []