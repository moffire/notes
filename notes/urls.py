"""notes URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.conf import settings
from landing.views import PostList, PostDetail, PostCreate, PostEdit, PostDelete

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('registration.backends.hmac.urls')),
    url(r'^$', PostList.as_view(), name='home_page'),
    url(r'^detail/(?P<slug>[\w-]+)/$', PostDetail.as_view(), name='post_details'),
    url(r'^create/', login_required(PostCreate.as_view()), name='post_create'),
    url(r'^edit/(?P<slug>[\w-]+)/$', PostEdit.as_view(), name='post_edit'),
    url(r'^delete/(?P<slug>[\w-]+)/$', PostDelete.as_view(), name='post_delete'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
